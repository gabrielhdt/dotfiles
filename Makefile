.POSIX:

MTREE = mtree
STOW  = stow

hierarchy:
	${MTREE} -cp shared | ${MTREE} -du -p ${HOME}
	${MTREE} -cp openbsd | ${MTREE} -du -p ${HOME}
	${MTREE} -cp linux | ${MTREE} -du -p ${HOME}

install:
	${STOW} shared
	${STOW} openbsd

doom-install:
	./post-emacs.scm
