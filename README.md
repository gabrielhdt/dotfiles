Dotfiles
========

Dependencies
------------

Deployment:

- GNU stow

Environment:

- feh
- ksh (openbsd)
- bash (linux)
- dunst
- stumpwm
- redshift
- 

Opening the session on `tty5` will automatically start
StumpWM if the Linux configuration has been deployed (see `linux/.profile`).

Editor
------

Doom emacs is used. Consequently, the `.emacs.d` is left free for Doom. All
personal configuration is done in [`.doom.d`](shared/.doom.d).
