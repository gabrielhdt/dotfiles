#!/usr/bin/env sh
exec guile -l download-dicts.scm -e 'main' -s "$0" "$@"
!#

(define (main args)
  (let ((dictionaries '("jmdict_english" "jmdict_french" "jmnedict"
                        "kanjidic_english" "kanjidic_french" "kireicake"))
        (base-url "https://foosoft.net/projects/yomichan/dl/dict/"))
    (for-each
     (lambda (dict)
       (system* "curl" (string-append base-url dict ".zip")
                "-o" (string-append dict ".zip")))
     dictionaries)))
