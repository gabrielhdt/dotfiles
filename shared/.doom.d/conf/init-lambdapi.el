;;; init-lambdapi.el --- Lambdapi mode settings -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2020 Gabriel Hondet
;;
;; Package-Requires: ((emacs 26.1) (quickrun) (lambdapi-mode))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Lambdapi mode settings.
;;
;;; Code:
(require 'lambdapi-mode)
(require 'quickrun)
(require 'rainbow-delimiters)
(require 'lispy)

(quickrun-add-command "lambdapi"
  '((:command . "lambdapi check")
    (:exec    . ("%c %s")))
  :mode 'lambdapi-mode)

(define-key lambdapi-mode-map (kbd "C-c r") #'quickrun)

(rainbow-delimiters-mode)
(lispy-mode)

(provide 'init-lambdapi)
;;; init-lambdapi.el ends here
