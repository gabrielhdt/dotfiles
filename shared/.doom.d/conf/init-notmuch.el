;;; init-notmuch.el --- Notmuch configuration -*- lexical-binding: t; -*-
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Notmuch is configured by ~/.notmuch-config. The ‘+notmuch-mail-folder’ must
;;  be consistent with the configuration. Hooks are set in notmuch so that
;;  ‘notmuch new’ automatically calls mbsync -a before and afew -a -t after (see
;;  ~/.mail/.notmuch/hooks).
;;
;;; Code:

(require 'init-email)
(require 'sendmail)

(setf +notmuch-sync-backend 'mbsync)
(setf +notmuch-mail-folder "~/.mail")
(setf +notmuch-sync-command "notmuch new")

;; Selecting the account in msmtp
;; https://notmuchmail.org/emacstips/#index11h2
(setf mail-specify-envelope-from t)
(setf message-sendmail-envelope-from 'header)
(setf mail-envelope-from 'header)


(provide 'init-notmuch)
;;; init-notmuch.el ends here
