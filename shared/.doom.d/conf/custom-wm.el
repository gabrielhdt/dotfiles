;;; package --- wm.el

;;; Commentary:
;;; Configuration of the Emacs X Window Manager

;;; Code:

; (use-package exwm)
(require 'exwm)
(require 'exwm-randr)
(require 'exwm-systemtray)
(require 'exwm-config)
(exwm-randr-enable)

;; Randr for multi screen
(exwm-config-default) ;don't use if custom config
(mouse-avoidance-mode 'exile)
; (display-battery-mode)
(setq ido-mode nil)

(require 'dash)

(defun md/exwm-set-monlist (mon pl)
  "Move workspaces specified in plist PL from primary display to MON."
  (setq exwm-randr-workspace-monitor-plist nil)
  (setq exwm-randr-workspace-monitor-plist
        (-concat (-interpose pl mon) `(,mon))))

(defun md/ext-screen-all (mon)
  "Move all workspaces to MON (except ws 0) and activate the provider."
  (interactive "sNew screen: ")
  (shell-command "xrandr --setprovideroutputsource 1")
  (shell-command "xrandr --VIRTUAL1 --off")
  (shell-command "xrandr --DP1 --off")
  (shell-command "xrandr --DP2 --off")
  (shell-command "xrandr --HDMI1 --off")
  (shell-command "xrandr --HDMI2 --off")
  (let ((pl '(1 2 3 4 5 6 7 8 9)))
    (md/exwm-set-monlist mon pl))
  (shell-command (concat "xrandr --" mon " --auto")))

(defun md/exwm-send-ws (mon)
  "Move all but one workspaces (ws 0) to screen MON."
  (interactive "sNew screen: ")
  (setq exwm-randr-workspace-monitor-plist
        `(1 ,mon 2 ,mon 3 ,mon 4 ,mon 5 ,mon 6 ,mon 7 ,mon 8 ,mon 9 ,mon)))

(defun md/exwm-send-zero (mon)
  "Move workspace 0 to screen MON."
  (interactive "sNew screen: ")
  (setq exwm-randr-workspace-monitor-plist
        `(0 ,mon)))
(provide 'wm)
;;; wm.el ends here
