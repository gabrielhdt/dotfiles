;;; init-email.el --- Misc email settings -*- lexical-binding: t; -*-
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Misc email settings.  Uses ‘sendmail’ to send emails.
;;
;;; Code:

(require 'message)
(require 'sendmail)
(setf send-mail-function 'sendmail-send-it)
(setf sendmail-program "msmtp")
(setf message-kill-buffer-on-exit t)

;; Reply line
(setq message-citation-line-format "On %a %d %b %Y at %R %f wrote:\n")
(setq message-citation-line-function 'message-insert-formatted-citation-line)

;; Encryption
(add-hook 'message-send-hook 'mml-secure-message-sign-pgpmime)

;; Confirm?
(add-hook 'message-send-hook
          (lambda ()
            (unless (yes-or-no-p "メールをよこしますか？")
              (signal 'quit nil))))
(provide 'init-email)
;;; init-email.el ends here
