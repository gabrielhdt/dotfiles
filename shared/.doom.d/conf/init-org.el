;;; ~/.dotfiles/emacs/.doom.d/conf/custom-org.el -*- lexical-binding: t; -*-

(setq org-highlight-latex-and-related '(latex script entities))
(add-to-list 'org-latex-classes
             '("koma-article"
               "\\documentclass{scrartcl}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\pargraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
