;;; setup-mu4e.el --- Sets up mu4e email client

;;; Commentary:

;;; Code:
(require 'init-email)
(require 'mu4e)
(require 'smtpmail)
(require 'starttls)
(require 'org-mu4e)
(setq mail-user-agent 'mu4e-user-agent)
(setq mu4e-maildir "~/Maildir")
(setq mu4e-get-mail-command "mbsync -a")
(setq mu4e-attachment-dir "~/Downloads/")
(setq mu4e-compose-dont-reply-to-self t)
(setq mu4e-user-mail-address-list
      '("gabrielhondet@gmail.com"
        "gabriel.hondet@alumni.enac.fr"
        "gabriel.hondet@laposte.net"
        "gabriel.hondet@lsv.fr"
        "gabriel.hondet@inria.fr"))
(setq smtpmail-queue-dir "~/Maildir/queue/cur")  ; if queuing is necessary
(setq mu4e-change-filenames-when-moving t)
(setq org-mu4e-convert-to-html nil)
;; (setq smtpmail-debug-info t)
;; (setq smtpmail-debug-verb t)

(setq org-capture-templates
      '(("t" "todo" entry (file+headline "~/auxync/todo.org" "Tasks")
         "* TODO [#A] %?\nSCHEDULED: %(org-insert-time-stamp
(org-read-date nil t)) \"+0d\"))\n%a\n")))

;; Regexp to define the folders, use 're-builder'
(defconst to-lsv
  "\\(\\(gabriel\.\\)?hondet\\|interne\\)@lsv\.fr\\|info1\\(8\\|9\\)\.dptinfo\.ens-cachan\.fr\\|deducteam-commits@lists\.gforge\.inria\.fr"
  "Emails concerning lsv, matches
   - gabriel.hondet@lsv.fr,
   - hondet@lsv.fr
   - info18.dptinfo.ens-cachan.fr")
(defconst to-gmail
  "\\(gabriel\.?hondet\\(+[^@]*\\)?@gmail\.com\\|gabriel\.hondet@alumni\.enac\.fr\\)"
  "Emails of google account, that is
   - gabrielhondet@gmail.com (and gabrielhondet+hata@gmail.com)
   - gabriel.hondet@alumni.enac.fr")
(defconst to-inria ".*@inria.fr"
  "Filters emails sent to inria account.")
(defconst to-laposte
  (rx (or "gabriel.hondet@laposte.net" "dedukti-dev@inria.fr"
          "deducteam@inria.fr"))
  "Filters emails sent to laposte.")

;; Dynamic trash folder
(setq mu4e-trash-folder
      (lambda (msg)
        ;; 'and msg' to handle the case when the msg is nil
        (cond
         ((and msg
               (mu4e-message-contact-field-matches
                msg '(:to :cc :bcc) to-gmail))
          "/gmail/[Gmail]/Bin")
         ((and msg
               (mu4e-message-contact-field-matches
                msg '(:to :cc :bcc) to-lsv))
          "/lsv/Trash")
         ((and msg
               (mu4e-message-contact-field-matches
                msg '(:to :cc :bcc) to-laposte))
          "/laposte/Trash")
         ((and msg
               (mu4e-message-contact-field-matches
                msg '(:to :cc :bcc) to-inria))
          "/inria/Trash")
         (t "/trash"))))

(setq mu4e-refile-folder
      (lambda (msg)
        ;; 'and msg' to handle the case when the msg is nil
        (cond
         ((and msg
               (mu4e-message-contact-field-matches
                msg '(:to :cc :bcc) to-gmail))
          "/gmail/[Gmail]/All Mail")
         ((and msg
               (mu4e-message-contact-field-matches
                msg '(:to :cc :bcc) to-lsv))
          "/lsv/Archive")
         ((and msg
               (mu4e-message-contact-field-matches
                msg '(:to :cc :bcc) to-laposte))
          "/laposte/archive")
         ((and msg
               (mu4e-message-contact-field-matches
                msg '(:to :cc :bcc) to-inria))
          "/inria/Archive")
         (t "/archive"))))

(setq mu4e-contexts
      `( ,(make-mu4e-context
           :name "gmail"
           :match-func (lambda (msg)
                         (when msg
                           (mu4e-message-contact-field-matches
                            msg :to to-gmail)))
           :vars '( ( user-mail-address . "gabrielhondet@gmail.com" )
                    ( user-full-name . "Gabriel Hondet" )
                    ( mu4e-sent-folder . "/gmail/[Gmail]/Sent Mail" )
                    ( smtpmail-smtp-user . "gabrielhondet" )
                    ( smtpmail-local-domain . "gmail.com" )
                    ( smtpmail-default-smtp-server . "smtp.gmail.com" )
                    ( smtpmail-smtp-server . "smtp.gmail.com" )
                    ( smtpmail-stream-type . starttls )
                    ( smtpmail-smtp-service . 587 )))
         ,(make-mu4e-context
           :name "ens-cachan.lsv"
           :match-func (lambda (msg)
                         (when msg
                           (mu4e-message-contact-field-matches
                            msg :to to-lsv)))
           :vars '( ( user-mail-address . "gabriel.hondet@lsv.fr" )
                    ( user-full-name . "Gabriel Hondet" )
                    ( mu4e-sent-folder . "/lsv/Sent" )
                    ( smtpmail-smtp-user . "hondet" )
                    ( smtpmail-local-domain . "lsv.ens-cachan.fr" )
                    ( smtpmail-default-smtp-server . "smtps.lsv.ens-cachan.fr" )
                    ( smtpmail-smtp-server . "smtps.lsv.ens-cachan.fr" )
                    ( smtpmail-stream-type . starttls )
                    ( smtpmail-smtp-service . 587 )))
         ,(make-mu4e-context
           :name "inria"
           :match-func (lambda (msg)
                         (when msg
                           (mu4e-message-contact-field-matches
                            msg :to to-inria)))
           :vars '( ( user-mail-address . "gabriel.hondet@inria.fr" )
                    ( user-full-name . "Gabriel Hondet" )
                    ( mu4e-sent-folder . "/inria/Sent" )
                    ( smtpmail-smtp-user . "ghondet" )
                    ( smtpmail-local-domain . "inria.fr" )
                    ( smtpmail-default-smtp-server . "smtp.inria.fr" )
                    ( smtpmail-smtp-server . "smtp.inria.fr" )
                    ( smtpmail-stream-type . starttls )
                    ( smtpmail-smtp-service . 587 )))
         ,(make-mu4e-context
           :name "laposte"
           :match-func (lambda (msg)
                         (when msg
                           (mu4e-message-contact-field-matches
                            msg :to "gabriel.hondet@laposte.net")))
           :vars '( ( user-mail-address . "gabriel.hondet@laposte.net" )
                    ( user-full-name . "Gabriel Hondet" )
                    ( smtpmail-smtp-user . "gabriel.hondet" )
                    ( mu4e-sent-folder . "/laposte/Sent" )
                    ( smtpmail-local-domain . "laposte.net" )
                    ( smtpmail-default-smtp-server . "smtp.laposte.net" )
                    ( smtpmail-smtp-server . "smtp.laposte.net" )
                    ( smtpmail-stream-type . ssl )
                    ( smtpmail-smtp-service . 465 ) ))))

;;; setup-mu4e.el ends here
