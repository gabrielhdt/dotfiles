;; -*- no-byte-compile: t; -*-
;;; .doom.d/packages.el

;;; Examples:
;; (package! some-package)
;; (package! another-package :recipe (:host github :repo "username/repo"))
;; (package! builtin-package :disable t)
(package! lambdapi-mode)
(package! flycheck-package)
(package! package-lint)
(package! exwm)
(package! gnuplot-mode)
(package! mingus)
(package! pandoc-mode)
(package! w3m)
