;;; $OpenBSD$
;;; .doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here
(add-to-list 'load-path (concat doom-private-dir "/conf/"))

;; Doom ui conf
(setf display-line-numbers-type 'relative)
