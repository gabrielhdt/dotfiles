(define-key *root-map* (kbd "P") "exec rofi -show drun")

(defvar *intl-layout* nil
  "Whether the layout is international or not. Used by @{switch-kbd-intl}.
  Sessions are usually started as non-intl.")

;; Switch between normal and international workman layout.
(defcommand switch-kbd-intl ()
  ()
  (if *intl-layout*
      (progn
        (setq *intl-layout* nil)
        (run-shell-command "setxkbmap us -variant workman"))
      (progn
        (setq *intl-layout* t)
        (run-shell-command "setxkbmap us -variant workman-intl"))))

(define-key *root-map* (kbd "C-i") "switch-kbd-intl")
