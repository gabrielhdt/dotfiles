#!/bin/bash
for f in ".profile" ".aliases"
do
    fpth="${HOME}/${f}"
    if [[ -r "$fpth" ]]
    then
        source "${fpth}"
    fi
done
# Update history
shopt -s histappend

dl_room () {
    xrandr --setprovideroutputsource 1 0
    xrandr --output DVI-I-1-1 --auto
    xrandr --output DVI-I-1-1 --left-of eDP-1
}
