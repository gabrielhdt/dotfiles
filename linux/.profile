#
# .profile    -- Commands executed by login shells (ksh,bash at least)
#

## Load system-wide profile
if [ -r /etc/profile ]; then
    . /etc/profile
fi

# Paths
# set PATH so it includes user's private bin if it exists
for bindir in "bin" ".local/bin" '.cask/bin'; do
    if [ -d "${HOME}/${bindir}" ]; then
        export PATH="${HOME}/${bindir}:${PATH}"
    fi
done

unset SSH_AGENT_PID
if [ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]; then
    export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
fi
export GPG_TTY=$(tty)
gpg-connect-agent UPDATESTARTUPTTY /bye >/dev/null


## Env variables
if [ -n "$(command -v most)" ]; then
    PAGER="most"
fi
VISUAL='emacsclient --create-frame'
EDITOR='emacsclient --create-frame --tty'
BROWSER=w3m
ALSA_CARD=PCH # Set default sound card
MANWIDTH=80
LESSHISTFILE='-'
TERM=xterm-256color
export VISUAL EDITOR BROWSER MANWIDTH LESSHISTFILE TERM PAGER


export _JAVA_AWT_WM_NONREPARENTING=1 # XMonad/java workaround

eval "$(opam env)"

[ -n "$(command -v mcron)" ] && [ -z "$(pidof mcron)" ] && mcron --daemon

# start stumpwm on tty6
if [ -z "${DISPLAY:-}" ] && [ "$(tty)" = '/dev/tty6' ]; then
    startx
fi
