#!/bin/bash

## TODO: set, in profile, an array mapping the exe name to a boolean
## with 1 if the exe exists, 0 otherwise.  Then only test for variable
## rather than calling which in .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

GPG_TTY=$(tty)
export GPG_TTY
gpg-connect-agent updatestartuptty /bye > /dev/null

# Set browser to use
if [[ -n "$DISPLAY" ]]; then
    if [[ -n "$(command -v firefox)" ]]
    then
        BROWSER=firefox
    else
        >&2 echo "No default X web browser set (no firefox)."
    fi
else
    if [[ -n "$(command -v w3m)" ]]; then
        BROWSER=w3m
    else
        >&2 echo "No terminal browser set (no w3m)."
    fi
fi
export BROWSER

if [[ -r /usr/share/bash-completion/bash_completion ]]
then
    . /usr/share/bash-completion/bash_completion
fi

fasd_cache="${HOME}/.fasd-init-bash"
if [[ "$(command -v fasd)" -nt "${fasd_cache}" || ! -s "${fasd_cache}" ]]; then
    fasd --init posix-alias bash-hook bash-ccomp bash-ccomp-install >| "${fasd_cache}"
fi
source "${fasd_cache}"
unset fasd_cache

[[ "$(command -v thefuck)" ]] && eval $(thefuck --alias)

## Prompt
red='\[\033[0;31m\]'
mag='\[\033[0;35m\]'

nor=0
bol='\[\033[1m\]'
dim=2
ita=3
und=4
bli=5
rev=7
inv=8
res='\[\033[0m\]'
# Prompt setup
function nz_ret() {
	RETVAL=$?
	[ $RETVAL -ne 0 ] && echo -e " ${RETVAL}"
}
PS1="${mag}${bol}\\W${res}${red}\$(nz_ret)${res} λ "
export PS1
