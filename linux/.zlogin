#!/bin/zsh
# Read at login, after .zshrc, used to launch programs that do not modify shell
# behaviour
set -euo pipefail

for bindir in "bin" ".local/bin" ".cask/bin" ; do
    if [[ -d "${HOME}/${bindir}" ]]; then
        export PATH="${HOME}/${bindir}:${PATH}"
    fi
done

SSH_ASKPASS="$(command -v ssh-askpass)"
export SSH_ASKPASS
gpg-connect-agent /bye > /dev/null

eval "$(opam env)"

export VISUAL='emacsclient --create-frame'
export EDITOR='emacsclient --create-frame --tty'

export _JAVA_AWT_WM_NONREPARENTING=1 # XMonad/java workaround

if [[ -n "$(command -v mcron)" ]] && [[ -z "$(pidof mcron)" ]]; then
    mcron --daemon
fi

if [[ -z "$DISPLAY" ]] && [[ "$(tty)" = '/dev/tty5' ]]; then
    exec xinit .xinitrc-exwm -- vt05
fi
if [[ -z "$DISPLAY" ]] && [[ "$(tty)" = '/dev/tty6' ]]; then
    exec xinit .xinitrc-stumpwm -- vt06
fi
