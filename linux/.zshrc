#!/bin/zsh

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

GPG_TTY=$(tty)
export GPG_TTY
SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
export SSH_AUTH_SOCK
gpg-connect-agent UPDATESTARTUPTTY /bye

zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' matcher-list '' '' '' 'r:|[._-]=** r:|=**'
zstyle :compinstall filename "${HOME}/.zshrc"
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.cache/zsh

## Guix
GUIX_PROFILE="${HOME}/.guix-profile"
. "$GUIX_PROFILE/etc/profile"

## Fasd
fasd_cache="${HOME}/.fasd-init-zsh"
if [[ "$(command -v fasd)" -nt "${fasd_cache}" || ! -s "${fasd_cache}" ]]
then
    fasd --init posix-alias zsh-hook >| "${fasd_cache}"
fi
source "${fasd_cache}"
unset fasd_cache

## Fuck
[[ "$(command -v thefuck)" ]] && eval "$(thefuck --alias)"

autoload -Uz compinit
compinit
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -v # vi style editing

alias ls='ls --color'
alias ll='ls -lArth'
alias ocaml="rlwrap -H ${HOME}/.history/ocaml ocaml"
alias ocamldebug="rlwrap -H ${HOME}/.history/ocamldebug ocamldebug"
alias vi="${EDITOR}"
alias ssh='TERM=xterm-256color ssh'

autoload -Uz promptinit
promptinit
prompt elite2
