#!guile
-e main -s
!#

(define ~ (getenv "HOME"))
(define emacs/confd (string-append ~ "/.emacs.d/"))
(define doom/exe (string-append emacs/confd "/bin/doom"))

(define (install-doom)
  ;; Install doom emacs
  (system* "git" "clone" "--depth" "1" "https://github.com/hlissner/doom-emacs"
           emacs/confd)
  (system* doom/exe "install" "--yes"))

(define (refresh-doom)
  ;; Refresh doom to take configuration into account
  (system* doom/exe "sync"))

(define (main _)
  (install-doom)
  (refresh-doom))
