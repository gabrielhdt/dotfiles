# sh/ksh initialization

PATH=$HOME/.local/bin:$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/games
ENV=$HOME/.kshrc
export PATH HOME TERM ENV

LC_CTYPE=en_US.UTF-8
LANG=en_US.UTF-8
export LC_CTYPE LANG

VISUAL='emacsclient --create-frame'
EDITOR='emacsclient --create-frame --tty'
BROWSER=w3m
export VISUAL EDITOR BROWSER

eval "$(opam env)"
