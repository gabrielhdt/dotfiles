#  -*- mode: shell-script; -*-
#
# .kshrc    -- Commands executed by each korn shell at startup
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Allow for ** and vi mode
set -o vi

GPG_TTY=$(tty)
export GPG_TTY
SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
export SSH_AUTH_SOCK
gpg-connect-agent UPDATESTARTUPTTY /bye >/dev/null

## Fasd
fasd_cache="${HOME}/.fasd-init-ksh"
if [[ "$(command -v fasd)" -nt "${fasd_cache}" || ! -s "${fasd_cache}" ]]
then
    fasd --init posix-alias posix-hook | \
        # ‘local’ does not exist in ksh, replace with ‘typeset’
        sed 's/local/typeset/' >| \
        "${fasd_cache}"
fi
. "${fasd_cache}"
unset fasd_cache

## Prompt
nz_ret() {
    retval=$?
    [[ $retval -ne 0 ]] && print " ${retval}"
    unset retval
}
mag='[35m'
red='[31m'
res='[00m'

PS1='${mag}${PWD##*/}${res}${red}$(nz_ret)${res}${res} & '
export PS1

## Aliases
alias ll='ls -lArth'
alias ssh='TERM=xterm-256color ssh'
